var express = require('express');
var app = express();

app.use(express.static('public'));

app.get('/', function(req,res){
  res.sendFile(__dirname + '/ethersjs.html');
});

app.get('/index.html', function(req,res){

  res.sendFile(__dirname + '/index.html');
});

app.get('/slider', function(req,res){

  res.sendFile(__dirname + '/slider.html');
});

var server = app.listen(8000, function(){
  console.log('Server is running... port 8000');
});
